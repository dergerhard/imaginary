package com.bitsfabrik.gerhardliebmann.imaginary.data;


import android.graphics.Bitmap;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

/**
 * Represents one tour according to web service http://api.foxtur.com/v1/tours/
 * and http://api.foxtur.com/v1/tours/{id}
 * e.g.
 * <p/>
 * <id>1</id>
 * <title>Rhino Tour</title>
 * <description>sdfsddsf</description>
 * <shortDescription>See Rhinos in their natural habitat</shortDescription>
 * <thumb>http://lorempixel.com/200/100/animals/1/</thumb>
 * <image>url</image>
 * <startDate>2015-05-21T14:15:24+02:00</startDate>
 * <endDate>2015-06-28T14:15:24+02:00</endDate>
 * <price>10.3</price>
 * <p/>
 * Created by Gerhard on 28.05.2015.
 */
//@Root(name="data")
//@ElementList
public class Tour implements Serializable {

    @SerializedName("id")
    private int mId;

    @SerializedName("title")
    private String mTitle;

    @SerializedName("shortDescription")
    private String mShortDescription;

    @SerializedName("description")
    private String mSDescription;

    @SerializedName("thumb")
    private String mThumbUrl;

    @SerializedName("image")
    private String mImageUrl;

    @SerializedName("startDate")
    private Date mStartDate;

    @SerializedName("endDate")
    private Date mEndDate;

    @SerializedName("price")
    private float mPrice;

    /**
     * Indicates, whether the details of the item are loaded
     */
    private boolean mDetailsLoaded=false;

    public Tour(int mId, String mTitle, String mShortDescription, String mSDescription, String mThumbUrl, String mImageUrl, Date mStartDate, Date mEndDate, float mPrice) {
        this.mId = mId;
        this.mTitle = mTitle;
        this.mShortDescription = mShortDescription;
        this.mSDescription = mSDescription;
        this.mThumbUrl = mThumbUrl;
        this.mImageUrl = mImageUrl;
        this.mStartDate = mStartDate;
        this.mEndDate = mEndDate;
        this.mPrice = mPrice;
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getShortDescription() {
        return mShortDescription;
    }

    public void setShortDescription(String mShortDescription) {
        this.mShortDescription = mShortDescription;
    }

    public String getDescription() {
        return mSDescription;
    }

    public void setDescription(String mSDescription) {
        this.mSDescription = mSDescription;
    }

    public String getThumbUrl() {
        return mThumbUrl;
    }

    public void setThumbUrl(String mThumbUrl) {
        this.mThumbUrl = mThumbUrl;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }

    public Date getStartDate() {
        return mStartDate;
    }

    public void setStartDate(Date mStartDate) {
        this.mStartDate = mStartDate;
    }

    public Date getEndDate() {
        return mEndDate;
    }

    public void setEndDate(Date mEndDate) {
        this.mEndDate = mEndDate;
    }

    public float getPrice() {
        return mPrice;
    }

    public void setPrice(float mPrice) {
        this.mPrice = mPrice;
    }

    public boolean getDetailsLoaded() {
        return mDetailsLoaded;
    }

    public void setDetailsLoaded(boolean mDetailsLoaded) {
        this.mDetailsLoaded = mDetailsLoaded;
    }

    @Override
    public String toString() {
        return "Tour{" +
                "mId=" + mId +
                ", mTitle='" + mTitle + '\'' +
                ", mShortDescription='" + mShortDescription + '\'' +
                ", mSDescription='" + mSDescription + '\'' +
                ", mThumbUrl='" + mThumbUrl + '\'' +
                ", mImageUrl='" + mImageUrl + '\'' +
                ", mStartDate=" + mStartDate +
                ", mEndDate=" + mEndDate +
                ", mPrice=" + mPrice +
                ", mDetailsLoaded=" + mDetailsLoaded +
                '}';
    }
}
