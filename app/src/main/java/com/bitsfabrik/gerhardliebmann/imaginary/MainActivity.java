package com.bitsfabrik.gerhardliebmann.imaginary;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.view.View;
import android.view.Window;

import com.bitsfabrik.gerhardliebmann.imaginary.data.Tour;
import com.bitsfabrik.gerhardliebmann.imaginary.data.TourAdapter;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.FragmentById;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Gerhard on 27.05.2015.
 */
@EActivity(R.layout.activity_main)
@OptionsMenu(R.menu.main_menu)
public class MainActivity extends AppCompatActivity implements TourAdapter.RecyclerViewItemClickListener {

    @ViewById(R.id.toolbar)
    Toolbar mToolbar;

    @FragmentById(R.id.frag_main)
    MainFragment mMainFragment;

    @FragmentById(R.id.frag_detail)
    DetailFragment mDetailFragment;


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setSupportActionBar(mToolbar);

        mDetailFragment.setViewMode(DetailFragment.ViewMode.AsFragmentNoContent);

        //set visibility of detail fragment
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            hideDetailFragment();

        } else {
            showDetailFragment();
        }
    }


    private void showDetailFragment() {
        if (mDetailFragment != null && mDetailFragment.getView().getVisibility() == View.GONE) {
            mDetailFragment.getView().setVisibility(View.VISIBLE);
        }
    }

    private void hideDetailFragment() {
        if (mDetailFragment != null && mDetailFragment.getView().getVisibility() == View.VISIBLE) {
            mDetailFragment.getView().setVisibility(View.GONE);
        }
    }


    /**
     * About us...
     */
    @OptionsItem(R.id.action_about)
    protected void aboutUsClicked() {
        Intent i = new Intent(this, AboutUsActivity_.class);
        startActivity(i);
    }

    /**
     * Item from list clicked (redirect from TourAdapter)
     * @param view
     * @param item
     */
    @Override
    public void onItemClicked(View view, Tour item) {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {

            Intent i = new Intent(this, DetailActivity_.class);
            Bundle b = new Bundle();
            b.putSerializable(DetailActivity.TOUR, item);
            i.putExtras(b);
            startActivity(i);
        } else {
            if (item.getDetailsLoaded()) {
                mDetailFragment.setData(item);
            } else {
                mDetailFragment.loadDetails(item.getId());
            }
            mDetailFragment.setViewMode(DetailFragment.ViewMode.AsFragment);
        }
    }


}
