package com.bitsfabrik.gerhardliebmann.imaginary;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.bitsfabrik.gerhardliebmann.imaginary.data.TourLoader;
import com.bitsfabrik.gerhardliebmann.imaginary.tabs.SlidingTabLayout;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Contains the Sliding Tab Layout and the detail view for landscape mode
 * Created by Gerhard on 29.05.2015.
 */
@EFragment(R.layout.fragment_main)
public class MainFragment extends Fragment {

    @ViewById(R.id.view_pager)
    protected ViewPager mPager;

    @ViewById(R.id.sliding_tab_layout)
    protected SlidingTabLayout mTabs;

    @Override
    public void onStart() {
        super.onStart();
        mPager.setAdapter(new ViewPagerAdapter(getChildFragmentManager()));
        mTabs.setDistributeEvenly(true);
        mTabs.setBackgroundColor(getResources().getColor(R.color.primary));
        mTabs.setSelectedIndicatorColors(getResources().getColor(R.color.accent));
        mTabs.setTabTextColor(getResources().getColor(R.color.primary_light));
        mTabs.setActiveTabTextColor(getResources().getColor(android.R.color.white));

        mTabs.setViewPager(mPager);
    }

    /**
     * ViewPagerAdapter holds the item list fragments
     */
    class ViewPagerAdapter extends FragmentPagerAdapter {

        private ItemListFragment_ frag1;

        private ItemListFragment_ frag2;

        private String[] mTabs;

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
            mTabs = getResources().getStringArray(R.array.tabs);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            try {
                return mTabs[position];
            } catch (Exception e) {
                return "No Title";
            }
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    if (frag1 == null) {
                        Bundle bundle = new Bundle();
                        bundle.putInt(TourLoader.LIST, TourLoader.LIST_ALL);
                        frag1 = new ItemListFragment_();
                        frag1.setArguments(bundle);
                        return frag1;
                    }
                    break;

                case 1:
                    if (frag2 == null) {
                        Bundle bundle = new Bundle();
                        bundle.putInt(TourLoader.LIST, TourLoader.LIST_TOP5);
                        frag2 = new ItemListFragment_();
                        frag2.setArguments(bundle);
                        return frag2;
                    }
                    break;

                default:
                    return frag1;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
