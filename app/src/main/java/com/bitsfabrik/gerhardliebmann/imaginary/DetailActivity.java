package com.bitsfabrik.gerhardliebmann.imaginary;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.bitsfabrik.gerhardliebmann.imaginary.data.Tour;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Gerhard on 28.05.2015.
 */
@EActivity(R.layout.activity_detail)
@OptionsMenu(R.menu.main_menu)
public class DetailActivity extends AppCompatActivity implements DetailFragment.ToolbarTitleListener {
    /**
     * Argument name
     */
    public static final String TOUR = "tour";

    @ViewById(R.id.toolbar)
    Toolbar mToolbar;

    /**
     * Fragment to display detail information
     */
    private DetailFragment mDetailFragment;

    /**
     * Id of the current detail item
     */
    private Tour mTour;


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mTour = (Tour)getIntent().getExtras().getSerializable(TOUR);

        //create the fragment
        if (findViewById(R.id.fragment_detail)==null) {
            mDetailFragment = new DetailFragment_();
            Bundle b = new Bundle();
            b.putSerializable(TOUR, mTour);
            mDetailFragment.setArguments(b);
            mDetailFragment.setToolbarTitleListener(this);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction()
                    .add(R.id.detail_main, mDetailFragment);
            ft.commit();
        }
    }

    /**
     * Home/back button
     */
    @OptionsItem(android.R.id.home)
    protected void home() {
        NavUtils.navigateUpFromSameTask(this);
    }

    @OptionsItem(R.id.action_about)
    protected void aboutUsClicked() {
        Intent i = new Intent(this, AboutUsActivity_.class);
        startActivity(i);
    }

    /**
     * Listener.. called from DetailFragment
     * @param title
     */
    @Override
    public void onSetToolbarTitle(String title) {
        mToolbar.setTitle(title);
    }


}
