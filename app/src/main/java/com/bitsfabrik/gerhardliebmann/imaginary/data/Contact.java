package com.bitsfabrik.gerhardliebmann.imaginary.data;

import com.google.gson.annotations.SerializedName;

/**
 * Company contact
 * Fields:
 * <companyName>Imaginary National Park</companyName>
 * <street>Random Street 42</street>
 * <country>Wonderland</country>
 * <phone>+436661234567</phone>
 * URL: http://api.foxtur.com/v1/contact/
 *
 * Created by Gerhard on 29.05.2015.
 */
public class Contact {

    @SerializedName("company")
    private String mCompanyName;

    @SerializedName("street")
    private String mStreet;

    @SerializedName("country")
    private String mCountry;

    @SerializedName("phone")
    private String mPhone;

    public Contact(String mCompanyName, String mStreet, String mCountry, String mPhone) {
        this.mCompanyName = mCompanyName;
        this.mStreet = mStreet;
        this.mCountry = mCountry;
        this.mPhone = mPhone;
    }

    public String getCompanyName() {
        return mCompanyName;
    }

    public void setCompanyName(String mCompanyName) {
        this.mCompanyName = mCompanyName;
    }

    public String getStreet() {
        return mStreet;
    }

    public void setStreet(String mStreet) {
        this.mStreet = mStreet;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String mCountry) {
        this.mCountry = mCountry;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String mPhone) {
        this.mPhone = mPhone;
    }
}
