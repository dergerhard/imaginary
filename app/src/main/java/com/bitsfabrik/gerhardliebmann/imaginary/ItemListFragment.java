package com.bitsfabrik.gerhardliebmann.imaginary;


import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;


import com.bitsfabrik.gerhardliebmann.imaginary.data.Tour;
import com.bitsfabrik.gerhardliebmann.imaginary.data.TourAdapter;
import com.bitsfabrik.gerhardliebmann.imaginary.data.TourLoader;
import com.bitsfabrik.gerhardliebmann.imaginary.misc.DividerItemDecoration;
import com.bitsfabrik.gerhardliebmann.imaginary.misc.Helper;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.List;

/**
 * Created by Gerhard on 27.05.2015.
 */
@EFragment(R.layout.fragment_item_list)
public class ItemListFragment extends Fragment implements TourAdapter.LoadDetailsListener {

    @ViewById(R.id.master_recycler_view)
    protected RecyclerView mRecyclerView;

    @ViewById(R.id.swipe_refresh)
    protected SwipeRefreshLayout mSwipeRefreshLayout;

    @App
    protected ImaginaryApplication mApp;

    /**
     * Manages tour data
     */
    private TourAdapter mAdapter;

    private int mWhichList;

    private boolean mDataLoaded = false;

    @Override
    public void onStart() {
        super.onStart();

        //set up adapter and recycler view
        mAdapter = new TourAdapter(getActivity(), null, this, (TourAdapter.RecyclerViewItemClickListener) getActivity());

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));

        mWhichList = getArguments().getInt(TourLoader.LIST);

        //load data from app class if already loaded
        if (mWhichList==TourLoader.LIST_ALL && mApp.getTourListAll()!=null) {
            setData(mApp.getTourListAll());
            mDataLoaded=true;
        }

        if (mWhichList==TourLoader.LIST_TOP5 && mApp.getTourListTop5()!=null) {
            setData(mApp.getTourListTop5());
            mDataLoaded=true;
        }

        //check if internet is available
        if (Helper.isNetworkAvailable(getActivity()) && !mDataLoaded) {
            loadTourList();
        } else if (!Helper.isNetworkAvailable(getActivity())){
            Toast.makeText(getActivity(), "No internet connection. Without internet this program won't display anything.", Toast.LENGTH_LONG).show();
        }

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadTourList();
            }
        });

    }


    /**
     * Sets the data for the adapter. @UiThread lets the method be executed on the UI thread (AsyncTask is on another thread)
     *
     * @param data
     */
    @UiThread
    public void setData(List<Tour> data) {
        mAdapter.setData(data);
        switch (mWhichList) {
            case TourLoader.LIST_ALL:
                mApp.setTourListAll(data);
                break;
            case TourLoader.LIST_TOP5:
                mApp.setTourListTop5(data);
                break;
        }
        mSwipeRefreshLayout.setRefreshing(false);
    }


    /**
     * Automatically creates an AsyncTask and loads the tour list
     */
    @Background
    public void loadTourList() {

        try {
            setData(TourLoader.loadTourList(mWhichList));
        }
        catch (Exception e) {
            Toast.makeText(getActivity(), "Couldn't load tour list", Toast.LENGTH_SHORT).show();
            Log.e("Imaginary", "Error loading tour list: " + e.toString());
        }
    }

    /**
     * preloading of detail view
     * @param tour
     */
    @Background
    @Override
    public void loadDetails(Tour tour) {
        try {
            TourLoader.loadDetails(getActivity(), tour);
        }
        catch (Exception e) {
            Log.e("Imaginary", "Couldn't load details: " + e.toString());
        }
    }

}
