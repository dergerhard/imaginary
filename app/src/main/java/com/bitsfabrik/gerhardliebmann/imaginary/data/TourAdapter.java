package com.bitsfabrik.gerhardliebmann.imaginary.data;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bitsfabrik.gerhardliebmann.imaginary.R;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;

/**
 * Created by Gerhard on 28.05.2015.
 */
public class TourAdapter extends RecyclerView.Adapter<TourAdapter.TourHolder> {

    private LayoutInflater mInflater;

    private Activity mActivity;

    private List<Tour> mData = Collections.emptyList();

    private RecyclerViewItemClickListener mItemClickedListener;

    private TourAdapter.LoadDetailsListener mDetailListener;

    private int lastPosition = -1;


    public TourAdapter(Activity context, List<Tour> data, TourAdapter.LoadDetailsListener loadListener, RecyclerViewItemClickListener clickListener) {
        mInflater = LayoutInflater.from(context);
        mData = data;
        mActivity = context;
        mDetailListener = loadListener;
        mItemClickedListener = clickListener;
    }

    @Override
    public TourHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.tour_row, parent, false);
        return new TourHolder(view);
    }


    @Override
    public void onBindViewHolder(TourHolder holder, int position) {
        if (mData != null) {
            Tour t = mData.get(position);

            //preload details
            mDetailListener.loadDetails(t);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            //set data (title, etc.)
            holder.mTitle.setText(t.getTitle());
            holder.mShortDescription.setText(t.getShortDescription());
            holder.mAvailableTill.setText(sdf.format(t.getEndDate()));
            holder.mPrice.setText(t.getPrice() + "");

            //load images and set them
            String url = t.getThumbUrl();
            if (url != null && url != "") {
                Picasso.with(mActivity).load(url).into(holder.mThumb);
            }

            setAnimation(holder.mContainerRow, position);
        }
    }

    /**
     * Sets the animation for a row
     * @param viewToAnimate
     * @param position
     */
    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mActivity, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public int getItemCount() {
        if (mData != null)
            return mData.size();
        else
            return 0;
    }


    public void setData(List<Tour> data) {
        this.mData = data;
        if (mData != null)
            notifyItemRangeChanged(0, mData.size());
        else
            notifyItemRangeChanged(0, 0);
    }


    /**
     * Container class for the view of one tour
     * show:
     * - small image
     * - short description
     * - available till
     * - price
     */
    protected class TourHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView mThumb;

        public TextView mTitle;

        public TextView mShortDescription;

        public TextView mAvailableTill;

        public TextView mPrice;

        private Context mContext;

        private LinearLayout mContainerRow;

        public TourHolder(View itemView) {
            super(itemView);
            mThumb = (ImageView) itemView.findViewById(R.id.thumb);
            mTitle = (TextView) itemView.findViewById(R.id.title);
            mShortDescription = (TextView) itemView.findViewById(R.id.short_description);
            mAvailableTill = (TextView) itemView.findViewById(R.id.available_till);
            mPrice = (TextView) itemView.findViewById(R.id.price);
            mContainerRow = (LinearLayout)itemView.findViewById(R.id.row);
            mContext = itemView.getContext();
            itemView.setOnClickListener(this);

        }

        /**
         * Redirects item clicks to the implementing fragment
         *
         * @param view
         */
        @Override
        public void onClick(View view) {
            if (mItemClickedListener != null) {
               mItemClickedListener.onItemClicked(view, mData.get(getLayoutPosition()));
            }
        }
    }

    /**
     * Interface for item clicks
     */
    public interface RecyclerViewItemClickListener {
        /**
         * Called, when an item is clicked. Its view and its ID are passed to the consumer
         *
         * @param view
         * @param item
         */
        void onItemClicked(View view, Tour item);
    }

    /**
     * Used to preload the details of a tour before displaying it
     */
    public interface LoadDetailsListener {
        void loadDetails(Tour t);
    }
}
