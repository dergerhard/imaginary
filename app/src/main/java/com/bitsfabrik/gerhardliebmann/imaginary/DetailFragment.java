package com.bitsfabrik.gerhardliebmann.imaginary;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bitsfabrik.gerhardliebmann.imaginary.data.Tour;
import com.bitsfabrik.gerhardliebmann.imaginary.data.TourLoader;
import com.bitsfabrik.gerhardliebmann.imaginary.data.rest.ImaginaryToursRest;
import com.bitsfabrik.gerhardliebmann.imaginary.data.rest.RestConfiguration;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.text.SimpleDateFormat;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by Gerhard on 28.05.2015.
 */
@EFragment(R.layout.fragment_detail)
public class DetailFragment extends Fragment {

    //image, title, description, bookable, call
    @ViewById(R.id.image)
    protected ImageView mImage;

    /**
     * Image shown if no item is selected
     */
    @ViewById(R.id.image_no_details)
    protected ImageView mImageNoDetails;

    /**
     * inserted to fit the material guidelines on design to match the sliding tab views color on the
     * detail pane
     */
    @ViewById(R.id.layout_material_buffer)
    protected LinearLayout mLayoutMaterialBuffer;

    @ViewById(R.id.detail_view)
    protected LinearLayout mDetailView;

    @ViewById(R.id.title)
    protected TextView mTitle;

    @ViewById(R.id.description)
    protected TextView mDescription;

    @ViewById(R.id.bookable)
    protected TextView mBookable;

    /**
     * Current item
     */
    private Tour mTour;

    /**
     * Reference to activity to set toolbar title
     */
    private DetailFragment.ToolbarTitleListener mListener;

    @Override
    public void onStart() {
        super.onStart();
        setViewMode(ViewMode.InStandaloneActivity);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        //if an itemID is in the arguments --> load it. otherwise do nothing.
        if (getArguments() != null) {
            mTour = (Tour) getArguments().getSerializable(DetailActivity.TOUR);
            if (mTour.getDetailsLoaded()) {
                setData(mTour);
            } else {
                loadDetails(mTour.getId());
            }
        }
    }

    @Click(R.id.call)
    public void onCallClick() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + ((ImaginaryApplication) getActivity().getApplication()).getContact().getPhone()));
        startActivity(callIntent);
    }

    /**
     * loads the details from REST Api
     *
     * @param id
     */
    @Background
    public void loadDetails(int id) {
        try {
            TourLoader.loadDetails(getActivity(), mTour);
            setData(mTour);
        }
        catch (Exception e) {
            Toast.makeText(getActivity(), "Could not load the details", Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * Applies the loaded details
     *
     * @param tour
     */
    @UiThread
    public void setData(Tour tour) {
        if (mListener != null)
            mListener.onSetToolbarTitle(tour.getTitle());

        mTour = tour;
        mTitle.setText(tour.getTitle());
        mDescription.setText(tour.getDescription());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd ");
        mBookable.setText(sdf.format(tour.getStartDate()) + " - " + sdf.format(tour.getEndDate()));

        Picasso.with(getActivity()).load(tour.getImageUrl()).into(mImage);
    }

    /**
     * Inject the listener for toolbar title
     *
     * @param listener
     */
    public void setToolbarTitleListener(DetailFragment.ToolbarTitleListener listener) {
        mListener = listener;
    }


    /**
     * Displays the fragment or the logo
     */
    public void setViewMode(ViewMode mode) {
        if (mode == ViewMode.AsFragment) {
            mLayoutMaterialBuffer.setVisibility(View.VISIBLE);
            mImageNoDetails.setVisibility(View.GONE);
            mDetailView.setVisibility(View.VISIBLE);
        }

        if (mode == ViewMode.AsFragmentNoContent) {
            mLayoutMaterialBuffer.setVisibility(View.VISIBLE);
            mImageNoDetails.setVisibility(View.VISIBLE);
            mDetailView.setVisibility(View.GONE);
        }

        if (mode == ViewMode.InStandaloneActivity) {
            mLayoutMaterialBuffer.setVisibility(View.GONE);
            mImageNoDetails.setVisibility(View.GONE);
            mDetailView.setVisibility(View.VISIBLE);
        }

    }


    /**
     * Interface for redirecting setToolbar calls
     */
    public interface ToolbarTitleListener {
        /**
         * Used to set the toolbar title
         */
        void onSetToolbarTitle(String title);
    }

    /**
     * Used to change the view accordingly.
     */
    public enum ViewMode {
        InStandaloneActivity, AsFragment, AsFragmentNoContent
    }
}
