package com.bitsfabrik.gerhardliebmann.imaginary.data.rest;

import com.bitsfabrik.gerhardliebmann.imaginary.data.Contact;
import com.bitsfabrik.gerhardliebmann.imaginary.data.Tour;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.Path;

/**
 * Interface that defines how RetroFit accesses the REST API
 * Created by Gerhard on 28.05.2015.
 */
public interface ImaginaryToursRest {

    @Headers({"Accept: application/json"})
    @GET("/v1/tours")
    List<Tour> getAllTours();

    @Headers({"Accept: application/json"})
    @GET("/v1/tours/top5")
    List<Tour> getTop5Tours();

    @Headers({"Accept: application/json"})
    @GET("/v1/tours/{id}?w=1080&h=600")
    Tour getTourDetails(@Path("id") int id);

    @Headers({"Accept: application/json"})
    @GET("/v1/contact")
    Contact getContactDetails();

}
