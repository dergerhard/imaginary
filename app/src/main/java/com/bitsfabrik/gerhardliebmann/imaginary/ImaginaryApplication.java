package com.bitsfabrik.gerhardliebmann.imaginary;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import com.bitsfabrik.gerhardliebmann.imaginary.data.Contact;
import com.bitsfabrik.gerhardliebmann.imaginary.data.Tour;
import com.bitsfabrik.gerhardliebmann.imaginary.data.TourLoader;
import com.bitsfabrik.gerhardliebmann.imaginary.misc.Helper;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EApplication;

import java.util.List;


/**
 * Created by Gerhard on 28.05.2015.
 */
@EApplication
public class ImaginaryApplication extends Application {

    private Contact mContact;

    private List<Tour> mTourListAll;

    private List<Tour> mTourListTop5;


    @Override
    public void onCreate() {
        if (Helper.isNetworkAvailable(this)) {
            loadContactInfo();
        }
    }

    /**
     * Load the contact information
     */
    @Background
    protected void loadContactInfo() {
        try {
            mContact = TourLoader.loadContactInfo();
        }
        catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Could not load contact details", Toast.LENGTH_SHORT);
            Log.e("Imaginary", "Could not load contact details: " + e.toString());
        }
    }

    /**
     * Method to access contact information
     * @return
     */
    public Contact getContact() {
        return mContact;
    }

    public List<Tour> getTourListAll() {
        return mTourListAll;
    }

    public void setTourListAll(List<Tour> mTourList) {
        this.mTourListAll = mTourList;
    }

    public List<Tour> getTourListTop5() {
        return mTourListTop5;
    }

    public void setTourListTop5(List<Tour> mTourList) {
        this.mTourListTop5 = mTourList;
    }

    public boolean areListsLoaded() {
        return mTourListAll!=null && mTourListTop5!=null;
    }
}
