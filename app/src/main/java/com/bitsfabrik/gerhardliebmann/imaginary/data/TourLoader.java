package com.bitsfabrik.gerhardliebmann.imaginary.data;

import android.content.Context;

import com.bitsfabrik.gerhardliebmann.imaginary.data.rest.ImaginaryToursRest;
import com.bitsfabrik.gerhardliebmann.imaginary.data.rest.RestConfiguration;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Implements the calls to the REST API. No Threads/AsyncTasks are started!
 * Created by Gerhard on 02.06.2015.
 */
public class TourLoader {

    //used to set which list to load
    public static final String LIST = "WhichList";
    public static final int LIST_ALL = 0;
    public static final int LIST_TOP5 = 1;

    /**
     * Load the tour list according to "whichList"
     * @param whichList
     * @return
     * @throws Exception
     */
    public static List<Tour> loadTourList(int whichList) throws Exception {
        //converter for date format
        //e.g.: 2015-05-21T17:46:31+02:00
        // date format according to http://developer.android.com/reference/java/text/SimpleDateFormat.html
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

        //create REST adapter
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(RestConfiguration.API_ENDPOINT)
                .setConverter(new GsonConverter(gson))
                .build();

        //create the REST service object
        ImaginaryToursRest service = restAdapter.create(ImaginaryToursRest.class);

        List<Tour> tours = Collections.emptyList();

        if (whichList == LIST_ALL) {
            tours = service.getAllTours();
        } else if (whichList == LIST_TOP5) {
            tours = service.getTop5Tours();
        }
        return  tours;
    }

    /**
     * Load the tour details
     * @param ctx the context
     * @param tour the tour (must have id)
     * @return
     */
    public static void loadDetails(Context ctx, Tour tour) {
        //converter for date format
        //e.g.: 2015-05-21T17:46:31+02:00
        // date format according to http://developer.android.com/reference/java/text/SimpleDateFormat.html
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

        //create REST adapter
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(RestConfiguration.API_ENDPOINT)
                .setConverter(new GsonConverter(gson))
                .build();

        //create the REST service object
        ImaginaryToursRest service = restAdapter.create(ImaginaryToursRest.class);

        //get the details
        Tour t = service.getTourDetails(tour.getId());

        //store additional details to existing object
        tour.setDescription(t.getDescription());
        tour.setImageUrl(t.getImageUrl());

        //preload detail image
        Picasso.with(ctx).load(tour.getImageUrl()).fetch();

        //tell the object it is loaded
        tour.setDetailsLoaded(true);
    }

    /**
     * Load the contact info
     * @return
     */
    public static Contact loadContactInfo() {
        //create REST adapter
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(RestConfiguration.API_ENDPOINT)
                .build();

        //create the REST service object
        ImaginaryToursRest service = restAdapter.create(ImaginaryToursRest.class);

        return service.getContactDetails();

    }
}
