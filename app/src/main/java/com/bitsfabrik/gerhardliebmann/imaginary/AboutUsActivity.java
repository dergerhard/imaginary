package com.bitsfabrik.gerhardliebmann.imaginary;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import com.bitsfabrik.gerhardliebmann.imaginary.data.Contact;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Gerhard on 29.05.2015.
 */
@EActivity(R.layout.activity_about_us)
@OptionsMenu(R.menu.about_menu)
public class AboutUsActivity extends AppCompatActivity{

    @ViewById(R.id.toolbar)
    protected Toolbar mToolbar;

    @ViewById(R.id.company_name)
    protected TextView mCompany;

    @ViewById(R.id.street)
    protected TextView mStreet;

    @ViewById(R.id.country)
    protected TextView mCountry;

    private Contact mContact;

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mToolbar.setTitle(getResources().getString(R.string.action_about));

        //get contact info from application
        mContact = ((ImaginaryApplication)getApplication()).getContact();

        if (mContact!=null) {
            mCompany.setText(mContact.getCompanyName());
            mStreet.setText(mContact.getStreet());
            mCountry.setText(mContact.getCountry());
        }
    }

    /**
     * Home/back button
     */
    @OptionsItem(android.R.id.home)
    protected void home() {
        finish();
    }


    @Click(R.id.call)
    public void onCallClick() {
        if (mContact!=null) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + mContact.getPhone()));
        startActivity(callIntent);
        }
        else {
            Toast.makeText(this, "No internet connection. Number not available", Toast.LENGTH_LONG).show();
        }
    }
}
